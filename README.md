# 1)Cricinfo
   Project Group 2D

   Bala Krishna Vijayagiri
   Hemanth Narne
   Rohith Kumar Agiru
   Srikar Reddy Katta
   
   Project pair 2-7
   Hemanth Narne
   Srikar Reddy Katta

   Project pair 2-8
   Bala Krishna Vijayagiri
   Rohith Kumar Agiru


# 2)Link to the public repository: https://bitbucket.org/Balakrishna_259/cricinfo

# 3)Overview of your project:
   Our project is regarding T20 cricket matches. The test data shows the detailed information of the matches with the teams played all over the world. It also gives the information like match id, series id description of the matches which gives the team names place and the date, the match has held. It also shows results who has won the match with their score details in each innings.

# 4)It is a structured data which is of size 1965 KB. The file extension is CSV. It has a total of 6418 records. The data included is from 2003 to 2017. 

# 5)Link to data: https://www.kaggle.com/nikhilshetty3/odicricketanalysis/data

# 6)Five V's of big data
   Volume: The data is all about T20 cricket matches information for each year from 2003 to 2017. It has over 6000 entries. The sources for the data is collected from various cricket boards. 
   Variety: The T20 matches data is a structured and is relational because of rows and columns.
   Velocity: The velocity of data is low because there will be maximum of 5 to 10 matches a day.
   Veracity: The given data looks clean as it is taken from the trustworthy sources.
   Value: By looking the test data we can get the number of runs scored by a team in a match, average winning runs of all the teams, number of overs played by a team. 

# 7)Big Data Questions

   1.For each country, find the number of mathches played.(Hemanth,Narne)  
   2.For each year, find the maximum first innings total.(Rohith)  
   3.For each year, find the average first innings score in each year.(Balakrishna)  
   4.For each stadium, find the total number of runs scored.(Srikar)  
   
   

# 8) Big Data Solutions  
   1.For each country, find the number of mathches played.(Hemanth,Narne)  
   ### Data Flow:
   Mapper Input: 65193	England  
   Mapper Output: Australia	220935  
   Reducer Output: Australia	137  
   ### Graph
   ![MatchesPlayedInACountry](Images/NumberOfMatchesPlayedInACountry.PNG)
   
   2.For each year, find the maximum first innings total.(Rohith)
   ### Data Flow:
   Mapper Input: 4	4	204	200	0	10	10	10	4	4.08	-1	-1	-1	0	Sri Lanka	England	England	Sri Lanka	Lord's	England	50	England	England	0	0	0	1	0	0	1	0	1	0	1   
   Mapper Output: 14/05/1999 1 204  
   Reducer Output: 359  
   ### Graph
   ![MaximumFirstInningsTotal](Images/Maximum-First-Innings-Total.PNG)
   
   3.For each year, find the average first innings score in each year.(Balakrishna)
   ### Data Flow:
   Mapper Input: 4	4	204	200	0	10	10	10	4	4.08	-1	-1	-1	0	Sri Lanka	England	England	Sri Lanka	Lord's	England	50	England	England	0	0	0	1	0	0	1	0	1	0	1    
   Mapper Output: 14/05/1999 1 204
   Reducer Output: 420      
   ### Graph
   ![AverageFirstInningsscore](Images/Average-First-Innings-Score.PNG)
      
   4.For each stadium, find the total number of runs scored.(Srikar)
   ### Data Flow:
   Mapper Input: 4	4	204	200	0	10	10	10	4	4.08	-1	-1	-1	0	Sri Lanka	England	England	Sri Lanka	Lord's	England	50	England	England	0	0	0	1	0	0	1	0	1	0	1    
   Mapper Output: 65193	Lord's  204
   Reducer Output: Aberdeen	62243.0   
   ### Graph
   ![AverageFirstInningsscore](Images/NumberOfRunsInEachStadium.PNG)
   NumberOfRunsInEachStadium.PNG
  
   iv) Language:
       Python

   v) chart:
      Bar chart.
