f= open('../Dataset.csv','r')
o=open('./mapperoutput.txt','w')
start = True
for data in f:
    i= data.strip().split(',')
    if len(i) == 38 and start:
        start = False
    elif len(i) == 38 and not start:
        Match,Date,Innings,Over,Runs,Total_Runs,Innings_Total_Runs,Runs_Remaining,Total_Out,Innings_Total_Out,Outs_Remaining,Wickets_in_Hand,Run_Rate,Innings_Run_Rate,Run_Rate_Required,Initial_Run_Rate_Required,Target_Score,DayNight,At_Bat,Fielding,Home_Team,Away_Team,Stadium,Country,Total_Overs,Winning_Team,Toss_Winner,at_bat_wins,at_bat_won_toss,at_bat_at_home,at_bat_bat_first,chose_bat_1st,chose_bat_2nd,forced_bat_1st,forced_bat_2nd,new_game,Error_In_Data,common_support = i
        o.write("{0}\t{1}\n".format(Country,Match))
f.close()
o.close()