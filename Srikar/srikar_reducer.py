s = open("sort.txt","r")
reduceroutput = open("reduce.txt", "w")

thisKey = ""
thisValue = 0.0

for line in s:
  data = line.strip().split('\t')
  Stadium, runs = data

  if Stadium != thisKey:
    if thisKey:
      # writes the final output key value pair
      reduceroutput.write(thisKey + '\t' + str(thisValue)+'\n')

    # Sets the new key
    thisKey = Stadium 
    thisValue = 0.0
  
  # sums the total runs in stadium
  thisValue += float(runs)

# write the output when dones
reduceroutput.write(thisKey + '\t' + str(thisValue)+'\n')

s.close()
reduceroutput.close()